var express = require('express');
var router = express.Router();
var pg = require('pg');
var async = require('async');
var fs = require('fs');
var path = require('path');

var SQL_DIR = path.join(__dirname, '../sql');
function getSQL(queryName) {
  return fs.readFileSync(path.join(SQL_DIR, queryName + '.sql'), 'utf8');
}

var sql = {
  topActiveUsers: getSQL('topActiveUsers'),
};

/**
 * GET Top Active Users
 */
router.get('/topActiveUsers', function (req, res) {
  var app = req.app;
  var configs = app.get('configs');
  var pgUrl = configs.POSTGRES_URL;
  pg.connect(pgUrl, function (err, client, done) {
    if (err) {
      res.status(500);
      return res.json({
        message: 'error connecting to DB',
        error: {},
        title: 'error'
      })
    }
    var pageNumber = parseInt(req.query.page) || 1;
    var limit = 10;//hardcoded for now
    var offset = (pageNumber - 1) * limit;

    var query = sql.topActiveUsers
      .replace('${limit}', limit)
      .replace('${offset}', offset)
    ;
    client.query(query, function (err, results) {
      if (err) {
        console.log(err);
        res.status(500);
        return res.json({
          message: 'error querying data',
          error: {},
          title: 'error'
        })
      }
      res.json(results.rows);
    });

  });
});

/**
 * GET User Details
 */
router.get('/users/:userId', function (req, res) {
  var app = req.app;
  var configs = app.get('configs');
  var pgUrl = configs.POSTGRES_URL;
  pg.connect(pgUrl, function (err, client, done) {
    if (err) {
      res.status(500);
      return res.json({
        message: 'Error connecting to DB',
        error: {},
        title: 'error'
      })
    }
    var userId = req.params.userId;
    var tempData = {};
    /** Need to make 4 queries, run these queries in parallel**/
    var parallelTasks = [];
    parallelTasks.push(function (next) {
      var query = 'SELECT id, name, created_at "createdAt" FROM users WHERE id=$1';
      client.query(query, [userId], function (err, results) {
        if (err) return next(err);
        if (results.rows.length != 1) {
          res.status(404);
          return res.json({
            message: 'User not found',
            error: {},
            title: 'error'
          })
        }
        tempData.userDetails = results.rows[0];
        next();
      });
    });
    parallelTasks.push(function (next) {
      var companiesQuery = 'SELECT  c.id,c.name,c.created_at as "createdAt",t.contact_user as "isContact"  FROM companies c INNER JOIN teams t ON t.company_id=c.id WHERE t.user_id=$1 LIMIT 5';
      client.query(companiesQuery, [userId], function (err, results) {
        if (err) return next(err);
        tempData.companies = results.rows;
        next();
      });
    });
    parallelTasks.push(function (next) {
      var listingsQuery = 'SELECT l.id,l.created_at as "createdAt",l.name,l.description FROM listings l WHERE created_by=$1 LIMIT 5';
      client.query(listingsQuery, [userId], function (err, results) {
        if (err) return next(err);
        tempData.createdListings = results.rows;
        next();
      });
    });
    parallelTasks.push(function (next) {
      var applicationsQuery = 'SELECT a.id,a.created_at as "createdAt", a.cover_letter as "coverLetter", to_json((SELECT x FROM (SELECT l.id, l.name,l.description) x)) AS listing FROM applications a INNER JOIN listings l ON a.listing_id=l.id WHERE a.user_id=$1 GROUP BY a.id, l.id LIMIT 5 ';
      client.query(applicationsQuery, [userId], function (err, results) {
        if (err) return next(err);

        tempData.applications = results.rows;
        next();

      });
    });

    async.parallel(parallelTasks, function (err) {
      if (err) {
        res.status(500);
        return res.json({
          message: 'Error in Querying',
          error: {},
          title: 'error'
        })
      }
      var userDetails = tempData.userDetails;
      userDetails.companies = tempData.companies;
      userDetails.createdListings = tempData.createdListings;
      userDetails.applications = tempData.applications;
      res.json(userDetails);
    });


  });
});

module.exports = router;

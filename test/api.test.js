var app = require('../app');
var request = require('supertest');
var pg = require('pg');
var chai = require('chai');
var expect = chai.expect;
var async = require('async');
var configs = app.get('configs');
var pgUrl = configs.POSTGRES_URL;
/**
 * Cleanup database before all tests
 */
function cleanDB(done) {
  pg.connect(pgUrl, function (err, client, close) {
    if (err) {
      close();
      done(err);
    }
    function truncateTable(tableName, next) {
      client.query(
        "TRUNCATE TABLE " + tableName + " RESTART IDENTITY CASCADE").on('end',
        next);
    }

    async.series([function (next) {
      truncateTable('users', next)
    }, function (next) {
      truncateTable('companies', next)
    }, function (next) {
      truncateTable('teams', next)
    }, function (next) {
      truncateTable('listings', next)
    }, function (next) {
      truncateTable('applications', next)
    }], function () {
      close();
      done();
    });
  })
}
function TestDataHelper(client) {
  var helper = {
    createUsers: function (userDetailList, cb) {
      var insertStrings = [];
      for (var i = 0; i < userDetailList.length; i++) {
        var userDetail = userDetailList[i];
        insertStrings.push("('" + userDetail.name + "')");
      }
      var values = insertStrings.join(",");
      client.query(
        "insert into users (name) values " + values + "RETURNING id, name, created_at",
        function (err, results) {
          if (err) {
            return cb(err);
          }
          return cb(null, results.rows);
        });
    },
    createListings: function (listingsDetailsList, cb) {
      var insertStrings = [];
      for (var i = 0; i < listingsDetailsList.length; i++) {
        var listingDetail = listingsDetailsList[i];
        insertStrings.push(
          "('" + listingDetail.created_by + "','" + listingDetail.name + "','" + listingDetail.description + "')");
      }
      var values = insertStrings.join(",");
      client.query(
        "insert into listings (created_by, name, description) values " + values + "RETURNING id, name, description, created_by, created_at",
        function (err, results) {
          if (err) {
            return cb(err);
          }
          return cb(null, results.rows);
        });
    },
    createApplications: function (applicationDetailsList, cb) {
      var insertStrings = [];
      for (var i = 0; i < applicationDetailsList.length; i++) {
        var applicationDetail = applicationDetailsList[i];
        insertStrings.push(
          "('" + applicationDetail.user_id + "','" + applicationDetail.listing_id + "')");
      }
      var values = insertStrings.join(",");
      client.query(
        "insert into applications (user_id, listing_id) values " + values + "RETURNING id, created_at, user_id, listing_id",
        function (err, results) {
          if (err) {
            return cb(err);
          }
          return cb(null, results.rows);
        });
    },
    createCompanies: function (companyDetailsList, cb) {
      var insertStrings = [];
      for (var i = 0; i < companyDetailsList.length; i++) {
        var companyDetails = companyDetailsList[i];
        insertStrings.push(
          "('" + companyDetails.name + "')");
      }
      var values = insertStrings.join(",");
      client.query(
        "insert into companies (name) values " + values + "RETURNING id, created_at, name",
        function (err, results) {
          if (err) {
            return cb(err);
          }
          return cb(null, results.rows);
        });
    },
    createTeams: function (teamDetailsList, cb) {
      var insertStrings = [];
      for (var i = 0; i < teamDetailsList.length; i++) {
        var teamDetails = teamDetailsList[i];
        insertStrings.push(
          "('" + teamDetails.user_id + "','" + teamDetails.company_id + "','" + teamDetails.contact_user + "')");
      }
      var values = insertStrings.join(",");
      client.query(
        "insert into teams (user_id,company_id,contact_user) values " + values + "RETURNING id,user_id,company_id,contact_user",
        function (err, results) {
          if (err) {
            return cb(err);
          }
          return cb(null, results.rows);
        });
    }

  };
  return helper;
}
function genUsers(count, prefix) {
  var data = [];
  for (var i = 0; i < count; i++) {
    data.push({
      name: prefix + i
    });
  }
  return data
}

function genListings(count, prefix, user) {
  var data = [];
  for (var i = 0; i < count; i++) {
    data.push({
      created_by: user.id,
      name: prefix + ' name' + i,
      description: prefix + ' description' + i
    });
  }
  return data
}

function genUserApplications(user, listings) {
  var data = [];
  for (var i = 0; i < listings.length; i++) {
    data.push({
      user_id: user.id,
      listing_id: listings[i].id
    });
  }
  return data
}

function genCompanies(count, prefix) {
  var data = [];
  for (var i = 0; i < count; i++) {
    data.push({
      name: prefix + i
    });
  }
  return data
}

function genUserTeams(user, companies, contactUserMap) {
  var data = [];
  for (var i = 0; i < companies.length; i++) {
    data.push({
      user_id: user.id,
      company_id: companies[i].id,
      contact_user: contactUserMap[companies[i].id] || false
    });
  }
  return data
}

beforeEach(cleanDB);
describe('Active Users API', function () {
  describe('GET /topActiveUsers', function () {
    beforeEach(function (done) {
      /**
       * Create some data
       */
      var tes = this;

      pg.connect(pgUrl, function (err, client, close) {
        function cancel(err) {
          close();
          done(err);
        }

        var td = TestDataHelper(client);
        td.createUsers(genUsers(17, 'User'), function (err, users) {
          if (err) return cancel(err);
          tes.users = users;
          td.createListings(genListings(20, 'Listing', users[0]),
            function (err, listings) {
              if (err) return cancel(err);
              tes.listings = listings;
              td.createApplications([{
                  user_id: users[3].id,
                  listing_id: listings[0].id
                }, {
                  user_id: users[3].id,
                  listing_id: listings[1].id
                }, {
                  user_id: users[3].id,
                  listing_id: listings[2].id
                }, {
                  user_id: users[3].id,
                  listing_id: listings[3].id
                }, {
                  user_id: users[1].id,
                  listing_id: listings[4].id
                }, {
                  user_id: users[1].id,
                  listing_id: listings[5].id
                }, {
                  user_id: users[0].id,
                  listing_id: listings[6].id
                }],
                function (err, applications) {
                  if (err) return cancel(err);
                  tes.applications = applications;
                  done();
                });
            });
        });
      });
    });
    it('should return first 10 Top Active Users', function (done) {
      var tes = this;
      request(app)
        .get('/topActiveUsers')
        .set('Accept', 'application/json')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function (err, res) {
          if (err) return done(err);
          expect(res.body).to.be.instanceOf(Array);
          expect(res.body).to.have.length(10);
          var user0 = res.body[0];
          expect(user0.id).to.equal(tes.users[3].id);
          expect(user0.createdAt).to.be.present;
          var user0CreatedAt = new Date(user0.createdAt);
          expect(user0CreatedAt).to.not.equal(NaN);
          expect(user0.count).to.equal(4);
          expect(user0.listings).to.be.instanceOf(Array);
          expect(user0.listings).to.have.length(3);


          var user1 = res.body[1];
          expect(user1.id).to.equal(tes.users[1].id);
          expect(user1.count).to.equal(2);
          expect(user1.listings).to.have.length(2);
          var user2 = res.body[2];
          expect(user2.id).to.equal(tes.users[0].id);
          expect(user2.count).to.equal(1);
          expect(user2.listings).to.have.length(1);

          /**
           * TODO: test order of listings by date (latest)
           */

          done();
        })
    });

    it('should return Top Active Users for current page', function (done) {
      var tes = this;
      request(app)
        .get('/topActiveUsers?page=2')
        .set('Accept', 'application/json')
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function (err, res) {
          if (err) return done(err);
          expect(res.body).to.be.instanceOf(Array);
          expect(res.body).to.have.length(7);
          var userList = res.body;
          for (var i = 0; i < userList.length; i++) {
            var userObj = userList[i];
            expect(userObj.id).to.not.equal(tes.users[3].id);
            expect(userObj.id).to.not.equal(tes.users[1].id);
            expect(userObj.id).to.not.equal(tes.users[0].id);
          }
          done();
        })
    });
  });

  describe('User Details API', function () {
    beforeEach(function (done) {
      /**
       * Create some data
       */
      var tes = this;

      pg.connect(pgUrl, function (err, client, close) {
        function cancel(err) {
          close();
          done(err);
        }

        var td = TestDataHelper(client);
        td.createUsers(genUsers(2, 'User'), function (err, users) {
          if (err) return cancel(err);
          tes.users = users;
          tes.user = users[0];
          tes.creator = users[1];
          td.createListings(genListings(10, 'Listing', tes.creator),
            function (err, listings) {
              if (err) return cancel(err);
              tes.listings = listings;
              td.createApplications(genUserApplications(tes.user, listings),
                function (err, applications) {
                  if (err) return cancel(err);
                  tes.applications = applications;
                  td.createCompanies(genCompanies(10, 'Company'),
                    function (err, companies) {
                      if (err) return cancel(err);
                      tes.companies = companies;
                      var cMap = {};
                      cMap[companies[0].id] = true;
                      td.createTeams(genUserTeams(tes.user, companies, cMap),
                        function (err, teams) {
                          if (err) return cancel(err);
                          tes.teams = teams;
                          done();
                        });
                    });
                });
            });
        });
      });
    });

    describe('GET /users/:userId', function () {
      it('should return the full details of the user', function (done) {
        var tes = this;
        request(app)
          .get('/users/' + tes.creator.id)
          .set('Accept', 'application/json')
          .expect(200)
          .expect('Content-Type', /json/)
          .end(function (err, res) {
            if (err) return done(err);
            var userDetails = res.body;
            expect(userDetails).to.be.instanceOf(Object);
            expect(userDetails.id).to.equal(tes.creator.id);
            expect(userDetails.name).to.equal(tes.creator.name);
            expect(userDetails.createdAt).to.be.present;

            var companies = userDetails.companies;
            expect(companies).to.be.instanceOf(Array);
            expect(companies).to.have.length(0);

            var createdListings = userDetails.createdListings;
            expect(createdListings).to.be.instanceOf(Array);
            expect(createdListings).to.have.length(5);

            var applications = userDetails.applications;
            expect(applications).to.be.instanceOf(Array);
            expect(applications).to.have.length(0);
            request(app)
              .get('/users/' + tes.user.id)
              .set('Accept', 'application/json')
              .expect(200)
              .expect('Content-Type', /json/)
              .end(function (err, res) {
                if (err) return done(err);
                var userDetails = res.body;
                expect(userDetails).to.be.instanceOf(Object);
                expect(userDetails.id).to.equal(tes.user.id);
                expect(userDetails.name).to.equal(tes.user.name);
                expect(userDetails.createdAt).to.be.present;

                var companies = userDetails.companies;
                expect(companies).to.be.instanceOf(Array);
                expect(companies).to.have.length(5);
                for (var i = 0; i < companies.length; i++) {
                  var company = companies[i];
                  expect(company.createdAt).to.be.present;
                  expect(company.isContact).to.be.present;
                }

                var createdListings = userDetails.createdListings;
                expect(createdListings).to.be.instanceOf(Array);
                expect(createdListings).to.have.length(0);
                for (var i = 0; i < createdListings.length; i++) {
                  var listing = createdListings[i];
                  expect(listing.createdAt).to.be.present;
                  expect(listing.name).to.be.present;
                  expect(listing.description).to.be.present;
                }

                var applications = userDetails.applications;
                expect(applications).to.be.instanceOf(Array);
                expect(applications).to.have.length(5);

                for (var i = 0; i < applications.length; i++) {
                  var application = applications[i];
                  expect(application.createdAt).to.be.present;
                  expect(application.coverLetter).to.be.present;
                  expect(application.listing).to.be.instanceOf(Object);
                  expect(application.listing.createdAt).to.be.present;

                }
                done();
              })
          })
      });
    });
  });
});
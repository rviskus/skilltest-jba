var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var _ = require('lodash');
var users = require('./routes/user');

var configs = require('./configs/default');
var env = process.env.NODE_ENV || 'development';
try {
  var envConfigs = require('./configs/envs/' + env);
  _.extend(configs, envConfigs);
}
catch (e) {
  //Do nothing
}


var app = express();
app.set('configs', configs);

app.locals.ENV = env;
app.locals.ENV_DEVELOPMENT = env == 'development';

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));


app.use('/', users);

/// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/// error handlers

// development error handler
// will print stacktrace

if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err,
      title: 'error'
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render({
    message: err.message,
    error: {},
    title: 'error'
  });
});


module.exports = app;
